
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
@NgModule({
  declarations: [

  ],
  imports: [
    NgbModule,
  ],
  providers: [],
  bootstrap: [],
  exports: [
    NgbModule,
  ]
})
export class SharedModule { }