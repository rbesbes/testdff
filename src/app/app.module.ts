import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared-module';
import { SideNavComponent } from './shared/side-nav/side-nav.component';
import { BicyclesProductsComponent } from './bicycles-products/bicycles-products.component';
import { CarouselComponent } from './bicycles-products/carousel/carousel.component';
import { FooterComponent } from './shared/footer/footer.component';
import { AboutComponent } from './bicycles-products/about/about.component';
import { FeaturedProductsComponent } from './bicycles-products/featured-products/featured-products.component';
import { MaintenanceProductsComponent } from './bicycles-products/maintenance-products/maintenance-products.component';
import { ProductsComponent } from './bicycles-products/maintenance-products/products/products.component';
import { LatestArticlesComponent } from './bicycles-products/latest-articles/latest-articles.component';
import { BicyclesCompanyComponent } from './bicycles-products/bicycles-company/bicycles-company.component';
import { SocialMediaComponent } from './bicycles-products/social-media/social-media.component';


@NgModule({
   declarations: [
      AppComponent,
      SideNavComponent,
      BicyclesProductsComponent,
      CarouselComponent,
      FooterComponent,
      AboutComponent,
      FeaturedProductsComponent,
      MaintenanceProductsComponent,
      ProductsComponent,
      LatestArticlesComponent,
      BicyclesCompanyComponent,
      SocialMediaComponent

   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      SharedModule
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
