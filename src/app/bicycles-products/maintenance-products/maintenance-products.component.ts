import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-maintenance-products',
  templateUrl: './maintenance-products.component.html',
  styleUrls: ['./maintenance-products.component.css']
})
export class MaintenanceProductsComponent implements OnInit {
  products = [{
    price: '$19.00',
    text: 'buy'
  },
  {
    price: '$33.00',
    text: 'buy'
  }]
  constructor() { }

  ngOnInit() {
  }

}
