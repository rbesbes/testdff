import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css'],
  providers: [NgbCarouselConfig]
})
export class CarouselComponent implements OnInit {
  time: number = 0;
  interval;

  images = ['/assets/images/1.jpg','/assets/images/2.jpg','/assets/images/3.jpg','/assets/images/4.jpg'];

    constructor(config: NgbCarouselConfig) {
      config.interval = 0;
      config.wrap = false;
    }
  

  ngOnInit() {
  }

}
